import { Link } from "gatsby";
import PropTypes from "prop-types";
import React, { Fragment } from "react";
import { TRIAL_TEXTS } from "../assets/data/languages";

const Trial = props => (
  <Fragment>
    <section className="text-center" id="trial">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-10 col-lg-8">
            <h2>{TRIAL_TEXTS.title[props.lang]}</h2>
            <p className="lead">
              {TRIAL_TEXTS.subtitle1[props.lang]}
              <br />
              {TRIAL_TEXTS.subtitle2[props.lang]}
            </p>
          </div>
        </div>
      </div>

      <div className="container demonstration">
        <div className="row">
          <div className="col-md-4">
            <div className="feature feature-1">
              <img
                alt="Students System Image"
                src="https://www.universis.gr/img/screenshots/students.png"
              />
              <div className="feature__body boxed boxed--border">
                <h5 className="demo-box">
                  {TRIAL_TEXTS.students[props.lang]}
                </h5>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="feature feature-1">
              <img
                alt="Teachers System Image"
                src="https://www.universis.gr/img/screenshots/teachers.png"
              />
              <div className="feature__body boxed boxed--border">
                <h5 className="demo-box">
                  {TRIAL_TEXTS.teachers[props.lang]}
                </h5>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="feature feature-1">
              <img
                alt="Registrar System Image"
                src="https://www.universis.gr/img/screenshots/registrar.png"
              />
              <div className="feature__body boxed boxed--border">
                <h5 className="demo-box">
                  {TRIAL_TEXTS.registrar[props.lang]}
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section className="text-center bg--secondary" id="participate">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-10 col-lg-8">
            <h2>{TRIAL_TEXTS.joinTitle[props.lang]}</h2>
            <p className="lead">
              {TRIAL_TEXTS.joinSubtitle[props.lang]}
              <br />
              <br />
              <strong>{TRIAL_TEXTS.dev[props.lang]}</strong>
            </p>
            <a
              className="btn btn--primary type--uppercase "
              href="https://gitlab.com/universis"
            >
              <span className="btn__text">
                {TRIAL_TEXTS.button1[props.lang]}
              </span>
            </a>
            <a
              className="btn btn--primary type--uppercase "
              href="https://universis.io/"
            >
              <span className="btn__text">
                {TRIAL_TEXTS.button2[props.lang]}
              </span>
            </a>
          </div>
        </div>
      </div>
    </section>
  </Fragment>
);

export default Trial;
