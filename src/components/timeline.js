import { Link } from "gatsby";
import PropTypes from "prop-types";
import React, { Fragment } from "react";
import { TIMELINE_TEXTS } from "../assets/data/languages";

const Timeline = props => (
  <Fragment>
    <section className="text-center">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-10 col-lg-8">
            <h2>{TIMELINE_TEXTS.title[props.lang]}</h2>
            <p className="lead">{TIMELINE_TEXTS.subtitle[props.lang]}</p>
          </div>
        </div>
      </div>
    </section>
    <section className="timeline-section">
      <div className="container">
        <div className="process-2 row">
          <div className="col-md-4">
            <div className="process__item">
              <h5>{TIMELINE_TEXTS.date1[props.lang]}</h5>
              <p>{TIMELINE_TEXTS.date1text[props.lang]}</p>
            </div>
          </div>

          <div className="col-md-4">
            <div className="process__item">
              <h5>{TIMELINE_TEXTS.date2[props.lang]}</h5>
              <p>{TIMELINE_TEXTS.date2text[props.lang]}</p>
            </div>
          </div>
          <div className="col-md-4">
            <div className="process__item">
              <h5>{TIMELINE_TEXTS.date3[props.lang]}</h5>
              <p>{TIMELINE_TEXTS.date3text[props.lang]}</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    {/* Timeline 2nd year */}
    <section className="timeline-section">
      <div className="container">
        <div className="process-2 row">
          <div className="col-md-4">
            <div className="process__item">
              <h5>{TIMELINE_TEXTS.date4[props.lang]}</h5>
              <p>{TIMELINE_TEXTS.date4text[props.lang]}</p>
            </div>
          </div>

          <div className="col-md-4">
            <div className="process__item">
              <h5>{TIMELINE_TEXTS.date5[props.lang]}</h5>
              <p>{TIMELINE_TEXTS.date5text[props.lang]}</p>
            </div>
          </div>
          <div className="col-md-4">
            <div className="process__item">
              <h5>{TIMELINE_TEXTS.date6[props.lang]}</h5>
              <p>{TIMELINE_TEXTS.date6text[props.lang]}</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  </Fragment>
);

export default Timeline;
